<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buycart extends CI_Controller {

	public function index()
	{
	
	}

	public function procesa(){
		$cliente = new Cliente();
		$cliente->nombre = $_POST["nombre"];
		$cliente->email = $_POST["email"];
		$cliente->direccion = $_POST["direccion"];
		$cliente->clave = "";
		$cliente->estado = "A";
		$cliente->save();

		// Grabar el pedido
		$pedido = new Pedido();
		$pedido->cliente_id = $cliente->id;
		$pedido->fecha_hora = date("d-m-Y H:i:s");
		$pedido->total = $_SESSION["compra"]["suma"];
		$pedido->estado = "P";
		$pedido->save();

		foreach ($_SESSION["compra"]["id"] as $key => $value) {
			$detalle = new Detallepedido();
			$detalle->id_pedido = $pedido->id;
			$detalle->id_producto = $value;
			$detalle->precio_u = $_SESSION["compra"]["precio"][$key];
			$detalle->cantidad = $_SESSION["compra"]["cantidad"][$key];
			$detalle->subtotal = $_SESSION["compra"]["subtotal"][$key];
			$detalle->save();
		}

	}


	public function ticket(){

		//echo "<pre>";print_r($_SESSION["compra"]);exit();

		$pdf = new Tablepdf('P','mm',array(75,150));
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(55,10,'TICKET DE PEDIDO',1,4,'C');
		//para el salto de Linea
		$pdf->Ln();
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(55,1,'FECHA: '.date("d-m-Y"),0,'L');

		$pdf->Ln(4);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(55,1,'CLIENTE: Miller Rivera Delgado',0,'L');

		$pdf->Ln(4);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(55,1,'DIRECCION:Jr: Sargento Lores 490',0,'L');

		$pdf->Ln(8);
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(9,5,'Cant.',1,0,'L');
		$pdf->Cell(12,5,'P.U.',1,0,'L');
		$pdf->Cell(27,5,'Producto',1,0,'L');
		$pdf->Cell(10,5,'Sub',1,0,'L');
		$pdf->Ln(5);

		$pdf->SetFont('Arial','',7);
		$pdf->SetWidths(array(9,12,27,10));


		foreach ($_SESSION["compra"]["id"] as $key => $value) {

			$pdf->Row(
				array(
					$_SESSION["compra"]["cantidad"][$key],
					$_SESSION["compra"]["precio"][$key],
					$_SESSION["compra"]["nombre"][$key],
					$_SESSION["compra"]["subtotal"][$key]	
				)
			);

			/*
			$pdf->Ln(3);
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(9,4,$_SESSION["compra"]["cantidad"][$key],0,0,'L');
			$pdf->Cell(12,5,$_SESSION["compra"]["precio"][$key],0,0,'L');
			$pdf->MultiCell(27,2,$_SESSION["compra"]["nombre"][$key],'L',1);
			$pdf->Cell(10,5,$_SESSION["compra"]["subtotal"][$key],0,0,'L');*/
		}
		$pdf->Cell(58,5,'TOTAL S/. '.$_SESSION["compra"]["suma"],1,0,'R');

		$pdf->Ln(10);

		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(55,1,'CUENTA INTERBANK: 213-1234123-215 ',0,'L');
		$pdf->Ln(3);

		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(55,1,'CUENTA BCP: 245-523434-5234 ',0,'L');
		$pdf->Ln(3);

		$pdf->Output();
	}

	public function confirmar(){
		
		$_SESSION["compra"] = $_POST;



		$this->load->view('template/header_web');
		$this->load->view('buycart/confirmar');
		$this->load->view('template/footer_web');
	
	}

	public function eliminar($position){
		unset($_SESSION["items"][$position]);
		redirect(base_url('buycart/listado'));
	}

	
	public function shop($id_producto){

		
		$_SESSION["items"][] = $id_producto;

		redirect(base_url("web/detalleproducto/".$id_producto));
	}


	public function listado(){

		$productos = array();
		$items = $_SESSION["items"];
		foreach ($items as $key => $value) {
			$producto = Producto_model::find($value);

			$productos[$key]["id"] = $producto->id;
			$productos[$key]["imagen"] = $producto->foto;
			$productos[$key]["precio"] = $producto->precio_venta;
			$productos[$key]["nombre"] = $producto->titulo;
		}

		$this->load->view("template/header_web");
		$this->load->view("buycart/listado",compact('productos'));
		$this->load->view("template/footer_web");
	}



}
