<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->login))
		{
			redirect('seguridad');
		}
	}

	
	public function index()
	{
		//echo "<pre>"; print_r($this->session->userdata());
		$this->load->view('template/header');
		$this->load->view('template/aside');
		$this->load->view('admin/index');
		$this->load->view('template/footer');
		
		
	}
}
