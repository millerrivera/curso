<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {


	public function index()
	{
        //total de registros
        $productos = Producto_model::all();
        $total_productos = count($productos);
       
        //Productos Actuales
        $take = 1;
        $skip = $this->uri->segment(3);
        //$limit = $total_productos - $skip; // the limit
        $productos_actuales = Producto_model::skip($skip)->take($take)->get();
        

        $this->load->library('pagination');

        $config['base_url'] = 'http://localhost/curso/web/index';
        $config['total_rows'] = $total_productos;
        $config['per_page'] = $take;

        $this->pagination->initialize($config);

        
        $this->load->view('template/header_web');
		$this->load->view('web/index',compact('productos_actuales'));
        $this->load->view('template/footer_web');
    }


    public function detalleproducto($id_producto){
        $producto =  Producto_model::Find($id_producto);

        if (isset($_SESSION["items"])){
            $busqueda = $_SESSION["items"];
            if (in_array($id_producto,$busqueda)) {
                $se_agrego = "SI";
            }else{
                $se_agrego = "NO";
            }
        }else{
            $se_agrego = "NO";
        }


        $this->load->view('template/header_web');
        $this->load->view('web/detalleproducto',compact('producto','se_agrego'));
        $this->load->view('template/footer_web');


    }
	
}
