<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguridad extends CI_Controller {


	public function index()
	{
		$this->load->view('seguridad/index');
	}

	public function login()
	{
		//echo "<pre>"; print_r($this->input->post());
		$mensaje="";
		$usuario = trim($this->input->post("usuario"));
		$password = trim($this->input->post("password"));
		$usuario_bd = Usuario_model::where('usuario','=',$usuario)->first();
		
		if($usuario_bd === null ){
			$mensaje= "Usuario es incorrecto";
		}
		else
		{
			if($usuario_bd->password == $password)
			{
				//crear sesion papu :v
				$array= array(
					'id' => $usuario_bd->id ,
					'usuario'=>$usuario_bd->usuario,
					'login' =>TRUE 
				);
				//guardamos la sesion
				$this->session->set_userdata($array);

				redirect("/admin");
			}
			else
			{
				$mensaje= "La clave es incorrecta";
				
			}
		}
		//echo $mensaje;
		$this->session->set_flashdata('mensaje',$mensaje);
		redirect("/seguridad");
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('seguridad');
	}
	
}
