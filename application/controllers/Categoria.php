<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->login))
		{
			redirect('seguridad');
		}
	}

	
	public function index()
	{
		
		$categoria= $this->datatables->new();
		$categoria->select('*')->from('categoria')->where('estado','A');
		$categoria
		->set_options('pagingType','\'full_numbers\'')
		->set_options('lengthMenu','[5,10,25,50]')
		->style(['class'=>'table table-striped table-bordered'])
		->column('ID CATEGORIA' , 'id')
		->column('DESCRIPCION' , 'descripcion')
		->column('ESTADO' , 'estado')
		->column('ACCIONES', 'id',function($data,$row){
			$html='<button onclick="editar('.$row['id'].')" class="btn btn-xs btn-warning" ><i class="far fa-edit"></i> </button> &nbsp'
				;
			$html .=	'<button onclick="eliminar('.$row['id'].')" class="btn btn-xs btn-danger" ><i class="fas fa-trash"></i></button>';
	
			return $html;

		})	;

		$this->datatables->init('categoria',$categoria);

		$this->load->view('template/header');
		$this->load->view('template/aside');
		$this->load->view('categoria/index');
     	$this->load->view('template/footer');
		
		
	}

	public function guardar()
	{
		
		
		$categoria = new categoria_model();
		$categoria->descripcion = $_POST["descripcionxd"];
		$categoria->estado = "A";	
		$categoria->save();
		

	}

	public function editarform($id)
	{
		$categoria =  categoria_model::Find($id);
		print json_encode($categoria->toArray());


	}

	public function eliminar($id)
	{
		$categoria =  categoria_model::Find($id);
		$categoria->estado = "I";
		$categoria->save();
		

	}
		public function editar($id)
	{
		$categoria =  categoria_model::Find($id);
		$categoria->descripcion = $_POST["descripcionxd"];
		$categoria->save();
		

	}

}
