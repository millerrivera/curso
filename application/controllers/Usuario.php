<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->login))
		{
			redirect('seguridad');
		}
	}

	
	public function index()
	{
		
		$usuario= $this->datatables->new();
		$usuario->select('*')->from('usuarios')->where('estado','A');
		$usuario
		->set_options('pagingType','\'full_numbers\'')
		->set_options('lengthMenu','[5,10,25,50]')
		->style(['class'=>'table table-striped table-bordered'])
		->column('ID USUARIO' , 'id')
		->column('NOMBRE' , 'usuario')
		->column('PASSWORD' , 'password')
		->column('ESTADO' , 'estado')
		->column('ACCIONES', 'id',function($data,$row){
			$html='<button onclick="editar('.$row['id'].')" class="btn btn-xs btn-warning" ><i class="far fa-edit"></i> </button> &nbsp'
				;
			$html .=	'<button onclick="eliminar('.$row['id'].')" class="btn btn-xs btn-danger" ><i class="fas fa-trash"></i></button>';
	
			return $html;

		})	;

		$this->datatables->init('usuario',$usuario);

		$this->load->view('template/header');
		$this->load->view('template/aside');
		$this->load->view('usuario/index');
     	$this->load->view('template/footer');
		
		
	}

	public function guardar()
	{
		
		
		$usuario = new usuario_model();
		$usuario->usuario = $_POST["usuarioxd"];
		$usuario->password = $_POST["password"];
		$usuario->estado = "A";	
		$usuario->save();
		

	}

	public function editarform($id)
	{
		$usuario =  usuario_model::Find($id);
		print json_encode($usuario->toArray());


	}

	public function eliminar($id)
	{
		$usuario =  usuario_model::Find($id);
		$usuario->estado = "I";
		$usuario->save();
		

	}
		public function editar($id)
	{
		$usuario =  usuario_model::Find($id);
		$usuario->usuario = $_POST["usuarioxd"];
		$usuario->password = $_POST["password"];
		$usuario->save();
		

	}

}
