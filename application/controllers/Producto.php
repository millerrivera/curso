<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->login))
		{
			redirect('seguridad');
		}
	}

	
	public function index()
	{
		#consulta paa listado de categorías:
		$categorias = Categoria_model::all()->where('estado','=','A');

		$producto= $this->datatables->new();
		$producto->select('producto.id as id, producto.titulo as titulo, producto.precio_venta as precio_venta, producto.fecha as fecha, producto.foto as foto, categoria.descripcion as categoria')
		->from('producto')
		->join('categoria','categoria.id = producto.id_categoria')
		->where('producto.estado','A');
		$producto
		->set_options('pagingType','\'full_numbers\'')
		->set_options('lengthMenu','[5,10,25,50]')
		->style(['class'=>'table table-striped table-bordered'])
		->column('ID PRODUCTO' , 'id')
		->column('CATEGORÍA','categoria' )
		->column('TÍTULO' , 'titulo')
		->column('PRECIO S/.' , 'precio_venta')
		->column('FECHA' , 'fecha')
		->column('FOTO' , 'foto',function($data,$row){
			$img= '<img src="'.base_url('assets/img/productos/').$row['foto'].'"  height="50" width="50" >'; 
			return $img;
		})
		->column('ACCIONES', 'id',function($data,$row){
			$html='<button onclick="editar('.$row['id'].')" class="btn btn-xs btn-warning" ><i class="far fa-edit"></i> </button> &nbsp';
				
			$html .='<button onclick="eliminar('.$row['id'].')" class="btn btn-xs btn-danger" ><i class="fas fa-trash"></i></button>';
	
			return $html;

		})	;

		$this->datatables->init('producto',$producto);

		$this->load->view('template/header');
		$this->load->view('template/aside');
		$this->load->view('producto/index',compact('categorias'));
     	$this->load->view('template/footer');
		
		
	}

	public function guardar(){
		;


		$producto = new Producto_model();
		$producto->titulo = $_POST["titulo"];
		$producto->descripcion = $_POST["descripcion"];
		$producto->precio_venta = $_POST["precio_venta"];
		$producto->id_categoria= $_POST["id_categoria"];
		$producto->estado = "A";
		$producto->fecha = date("y-m-d");
		

		//----------------------------------------------------------------------------------
		//configuracion de la libreria de imágen :V

				$config['upload_path']          = './assets/img/productos/';
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        print json_encode($error);

                        
                }
                else
                {
                        $data =  $this->upload->data("file_name");
                        $producto->foto = $data;
                        $producto->save();
                        $envio = array("error"=>"no");
                        print json_encode($envio);

 	             }
 	             $producto->save();
 	    //------------------------------------------------------------------------------------

	}

		public function editar($id)
	{
		$producto =  producto_model::Find($id);
		$producto->titulo = $_POST["titulo"];
		$producto->descripcion = $_POST["descripcion"];
		$producto->precio_venta = $_POST["precio_venta"];
		$producto->id_categoria = $_POST["id_categoria"];


		$config['upload_path']          = './assets/img/productos/';
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['encrypt_name'] = TRUE;//genera un codigo para cada imágen subida , sin importar si es la misma, se base en la fecha y hora de subido el archivo.

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        print json_encode($error);

                        
                }
                else
                {
                        $data =  $this->upload->data("file_name");
                        $producto->foto = $data;
                        $producto->save();
                        $envio = array("error"=>"no");
                        print json_encode($envio);

 	             }
 	  		$producto->save();

		

	}


	public function editarform($id)
	{
		$producto =  producto_model::Find($id);
		print json_encode($producto->toArray());


	}


	public function eliminar ($id){

		$producto =  Producto_model::Find($id);
		$producto->estado = "I";
		$producto->save();
		
	}

}