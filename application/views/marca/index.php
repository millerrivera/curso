<!Doctype html>
<!DOCTYPE html>
<html>
<head>
	<title>Listado Marca</title>
</head>
	<a href="<?php echo base_url('marca/nuevo') ?>">Nuevo registro</a><br/> <br/> 

<body>
	<table class="table" border="4" >
		<thead>
			<tr>
				<th>Id</th>
				<th>Descripcion</th>
				<th>Estado</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<?php foreach ($marca as $key => $value): ?>
				<td><?php echo $value->id; ?></td>
				<td><?php echo $value->descripcion; ?></td>
				<td><?php echo $value->estado; ?></td>	
				<td> 
					<a href="<?php echo  base_url('marca/editarform/').$value->id ?>">editar</a>
					<a href="<?php echo  base_url('marca/eliminar/').$value->id ?>">eliminar</a>

				</td>
			</tr>
			<?php endforeach ?>	
		</tbody>
	</table>

</body>
</html>
