
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2 >Productos</h2>
       
      </div>
      <button type="button" class="btn btn-success " id="agregar">Agregar</button><br><br>


      <div class="table-responsive">
       
       <?php $this->datatables->generate('producto');?>
      </div>
      <?php $this->datatables->jquery('producto');?>
    </main>






  <div class="modal fade" id="modal-producto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Formulario producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="form-producto" method="POST" action="<?php echo base_url('producto/guardar');?>" enctype="multipart/form-data" >
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">producto:</label>
            <input type="text" class="form-control" id="titulo" name="titulo">
            <input type="hidden" name="id" id="id">
          </div>

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Descripción:</label>
            <textarea class="form-control" id="descripcion" name="descripcion" rows="5"></textarea> 
          </div>

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Precio:</label>
            <input type="text" class="form-control" id="precio_venta" name="precio_venta">
          </div>

          <div class="form-group" >
            <label for="categoria">Categorías:</label>
            <select id="id_categoria" name="id_categoria" class="form-control">
              <?php foreach ($categorias as $row => $val): ?>
                <option value="<?php echo $val->id?>" ><?php echo $val->descripcion?></option>
                
              <?php endforeach ?>
            </select>
          </div>

         
          <div class="form-group ">
            <label for="recipient-name" class="col-form-label">Foto:</label>
            <div class="custom-file">
              
              <label class="custom-file-label" for="customFileLang" >Seleccionar Archivo</label>
              <input type="file" class="custom-file-input"  id="foto" name="foto" >
            </div>
           </div>

           <!--<div class="custom-file">
            <input type="file" class="custom-file-input" id="customFileLang" lang="es">
            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
            </div>-->
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button"  class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" id="grabar-producto" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $("#agregar").on('click',function(){
      $("#id").val("");
      $("#form-producto").trigger('reset');
      $("#modal-producto").modal('show');
    })

     $("#grabar-producto").on( 'click',function(){

      var idproducto = $("#id").val();
      
      var form = $("#form-producto")[0];
      var frm = new FormData(form);
      if (idproducto==""){
        $.ajax({
        url:$("#form-producto").attr('action'),//a donde viaja 
        type: 'POST', 
        data: frm,//envia todos los inputs del formulario
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
			//alertify.success('Se agregó correctamente :v	');
        var result = jQuery.parseJSON(respuesta);
        if(result.error=="no"){
             $("#modal-producto").modal('hide');
             $("#form-usuario").trigger('reset');
            Swal.fire(
            'Se guardó correctamente',
            'Un nuevo producto se agregó satisfactoriamente',
            'success'
            )

             alertify.success('Eres un crack papu :3');


        }
        else{
           Swal.fire(
            'Error al guardar: ',
            'Error de formato, tamaño o peso de la foto: '+ result.error,
            'error'
            )
             alertify.error('Haz bien las cosas, inútil xd');

        }$('#producto').dataTable().fnDraw();
      }
      
      })
    }
    else
    {
       $.ajax({
         url: '<?php echo base_url("producto/editar/")?>'+idproducto,
        type: 'POST', 
        data: frm,//envia todos los inputs del formulario
        cache: false,
        contentType: false,
        processData: false,

        success: function(respuesta){
      //alertify.success('Se agregó correctamente :v  ');
         var result = jQuery.parseJSON(respuesta);
           if(result.error=="no"){
             $("#modal-producto").modal('hide');
             $("#form-usuario").trigger('reset');
            Swal.fire(
            'Se guardó correctamente',
            'El producto se editó satisfactoriamente',
            'success'
            )
             alertify.success('Eres un crack papu :3');


           }
           else{
           Swal.fire(
            'Error al guardar: ',
            'Error de formato, tamaño o peso de la foto:'+ result.error,
            'error'
            )
             alertify.error('Haz bien las cosas, inútil xd');


           }$('#producto').dataTable().fnDraw();

        }
      })
     
    }          

    });
  });



  function editar(id){
  	$.ajax({
		url: '<?php echo base_url("producto/editarform/")?>'+ id,
		type: 'GET',
		success: function(respuesta){
			console.log(respuesta);
      $('#titulo').val(respuesta.titulo);
			$('#descripcion').text(respuesta.descripcion);
      $('#precio_venta').val(respuesta.precio_venta);
      $('#id_categoria').val(respuesta.id_categoria);

      $("#id").val(respuesta.id);
      
			$("#modal-producto").modal('show');

		},
		dataType: "json"
	})

  };



    function eliminar(id){
      $.ajax({
        url: '<?php echo base_url()?>producto/eliminar/'+id,
        type: 'GET',
        success: function(respuesta){
                console.log(respuesta);
                alertify.success('Sí se eliminó alv xdxd que shido papu ');

          $('#producto').dataTable().fnDraw();


        }
      })
        
    }

  



</script>


