<div class="container">
  <div class="row" >
    <div class="col-md-3">
      CATEGORIAS
    </div>
    <div class="col-md-9" >
      <?php foreach ($productos_actuales as $key => $value): ?>
      <div class="row">
        <div class="col-md-3" >
            <img width="70px" height="70px" src="<?php echo base_url() ?>assets/img/productos/<?php echo $value->foto; ?>" >
        </div>
        <div class="col-md-9" >
          <h4><a href="<?php echo base_url() ?>web/detalleproducto/<?php echo $value->id; ?>" ><?php echo $value->titulo; ?><a></h4>
          <h5><?php echo $value->precio_venta; ?></h5>
        </div>
      </div>
      <hr>
      <?php endforeach ?>
      <?php echo $this->pagination->create_links(); ?>
    </div>
  </div>
</div>
