<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" >
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fontawesome/css/all.css" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/signin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/dashboard.css">




    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="shortcut icon" href="<?php print base_url(); ?>assets/img/logo2.png">



    <title> | Curso </title>

  </head>
  
  <body class="text-center " style="background: url('<?php print base_url(); ?>assets/img/e.jpg');background-size: cover;" >
    
    <form class="form-signin " method="POST" action="<?php echo base_url('seguridad/login') ?>">
  <img class="mb-4" src="<?php echo base_url() ?>/assets/img/logo.png" alt="" width="72" height="72">
  <h1 class="h3 mb-3 font-weight-normal"> <b><font color="black">INGRESA!</font></b></h1>
  <label for="inputEmail" class="sr-only " >Usuario</label>
  <input type="text" id="usuario" name="usuario" class=" form-control " placeholder="Usuario" required autofocus>
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
 
  <button class="btn btn-lg btn-success btn-block" type="submit">Iniciar sesión</button>
  <?php if($this->session->flashdata('mensaje')):?>
  
  <h4><p class="mt-5 mb-3 text-danger"><strong><?php echo $this->session->flashdata('mensaje');?></strong></p></h4>
<?php endif;?>
  
  <h5><p class="mt-5 mb-3 text-muted"><font color="skyblue  "> &copy; 2019</font></p></h5>
</form>


  
  



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   
  </body>


</html> 