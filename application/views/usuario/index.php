
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2 >Usuarios</h2>
       
      </div>
      <button type="button" class="btn btn-success " id="agregar">Agregar</button><br><br>


      <div class="table-responsive">
       
       <?php $this->datatables->generate('usuario');?>
      </div>
      <?php $this->datatables->jquery('usuario');?>
    </main>






  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Formulario usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="form-usuario" method="POST" action="<?php echo base_url('usuario/guardar');?>" >
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Usuario:</label>
            <input type="text" class="form-control" id="usuarioxd" name="usuarioxd">
            <input type="hidden" name="id" id="id">
          </div>

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Password:</label>
            <input type="text" class="form-control" id="password" name="password">
          </div>
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="eliminar-usuario" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" id="grabar-usuario" class="btn btn-success">Guardad</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $("#agregar").on('click',function(){
      $("#id").val("");
      $("#form-usuario").trigger('reset');
      $("#exampleModal").modal('show');
    })

     $("#grabar-usuario").on( 'click',function(){

      var idusuario = $("#id").val();
      if (idusuario==""){
        $.ajax({
        url:$("#form-usuario").attr('action'),//a donde viaja 
        type: 'POST', 
        data: $("#form-usuario").serialize(),//envia todos los inputs del formulario
        success: function(respueta){
			//alertify.success('Se agregó correctamente :v	');
			
            Swal.fire({
           position: 'top-end',
           type: 'success',
           title: 'Se guardó correctamente',
           showConfirmButton: false,
           timer: 1500
            })
      alertify.success('Que shido papu');
			$("#exampleModal").modal('hide');
			$("#form-usuario")[0].reset();//o tambien se puede usar $("#form-usuario").trigger('reset')
        }
      })
    }
    else
    {
       $.ajax({
         url: '<?php echo base_url("usuario/editar/")?>'+idusuario,
        type: 'POST', 
        data: $("#form-usuario").serialize(),//envia todos los inputs del formulario
        success: function(respueta){
      //alertify.success('Se agregó correctamente :v  ');
      
      Swal.fire({
      position: 'top-end',
       type: 'success',
      title: 'Se guardó correctamente',
      showConfirmButton: false,
      timer: 1500
      })
      alertify.success('Que shido papu');
      $("#exampleModal").modal('hide');
      $("#form-usuario")[0].reset();//o tambien se puede usar $("#form-usuario").trigger('reset')
        }
      })
     
    }
    $('#usuario').dataTable().fnDraw();
    });
  });



  function editar(id){
  	$.ajax({
		url: '<?php echo base_url("usuario/editarform/")?>'+ id,
		type: 'GET',
		success: function(respuesta){
			console.log(respuesta);
      $('#usuarioxd').val(respuesta.usuario);
			$('#password').val(respuesta.password);
      $("#id").val(respuesta.id);
      
			$("#exampleModal").modal('show');

		},
		dataType: "json"
	})

  };

    function eliminar(id){
      $.ajax({
        url: '<?php echo base_url()?>usuario/eliminar/'+id,
        type: 'GET',
        success: function(respuesta){
                console.log(respuesta);
                alertify.success('Sí se eliminó alv xdxd que shido papu ');

          $('#usuario').dataTable().fnDraw();


        }
      })
        
    }

  



</script>


