<div class="container">
	<div class="row">
		<form method="POST" id="form-pedidos" action="<?php echo base_url('buycart/confirmar') ?>" >	
		<table class="table table-borderer table-striped" >
			<thead>
				<tr>
					<th>N°</th>
					<th>FOTO</th>
					<th>PRODUCTO</th>
					<th>PRECIO</th>
					<th>CANTIDAD</th>
					<th>SUBTOTAL</th>
					<th>.:.</th>
				</tr>
			</thead>
			<tbody>
				<?php $suma = 0; ?>
				<?php foreach ($productos as $key => $value): ?>

					<?php $suma = (double)$suma+(double)$value["precio"]; 
						$suma = number_format($suma, 2, '.', ' ');
					?>


					<tr>
						<td>
		<input type="hidden" name="id[]" value="<?php echo $value["id"]; ?>" >
							<?php echo $key+1; ?>
								
						</td>
						<td><img width="60px" height="60px" src="<?php echo base_url() ?>assets/img/productos/<?php echo $value['imagen']; ?>"
							>
						</td>
						<td>
<input type="hidden" name="nombre[]" value="<?php echo $value["nombre"] ?>" >

							<?php echo $value["nombre"] ?>
								
							</td>
						<td>
<input type="hidden" name="precio[]" value="<?php echo $value["precio"] ?>" >
							<?php echo $value["precio"] ?>
								
						</td>
						<td>
							<input type="text" class="cantidad" name="cantidad[]" value="1" >
						</td>
						<td><input type="text" name="subtotal[]" 
							value="<?php echo $value["precio"] ?>" readonly=""></td>
						<td>
<a href="<?php echo base_url('buycart/eliminar/'.$key) ?>" role="button" class="btn btn-sm btn-danger" >
								<i class="fas fa-trash-alt"></i>
							</a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5" style="text-align: right;" >
						<b>TOTAL A PAGAR:</b>
						<input type="hidden" id="suma" name="suma" value="<?php echo $suma; ?>">
					</td>
					<td id="imprime_suma" >
						<?php echo $suma; ?>
							
					</td>
				</tr>
			</tfoot>
		</table>
		</form>
	</div>

	<div class="row justify-content-md-center">
		
			<button id="pagar" class="btn btn-primary" type="button" >PAGAR AHORA</button>
		
	</div>

	<div class="row">
		<pre>
		<?php print_r($_SESSION); ?>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/buycart.js?v=12') ?>" ></script>