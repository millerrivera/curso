<div class="container" >
	<div class="row">
		<div class="col-md-6">
			<h3>DATOS DEL CLIENTE</h3>
			<form method="POST" id="form-cliente" action="<?php echo base_url('buycart/procesa') ?>" >

				<div class="form-group" >
					<label class="text-info" >Nombres y Apellidos</label>
					<input type="text" class="form-control" id="nombre" name="nombre" >
				</div>


				<div class="form-group" >
					<label class="text-info" >Email</label>
					<input type="email" class="form-control" id="email" name="email" >
				</div>

				<div class="form-group" >
					<label class="text-info" >Dirección</label>
					<input type="text" class="form-control" id="direccion" name="direccion" >
				</div>

				<div class="form-group" >
				<button class="btn btn-primary" type="button" id="genera" >Generar Ticket de Compra</button>
				</div>
				
			</form>
		</div>
		<div id="div-ticket" class="col-md-6">
				
				<iframe  width="560" height="315" src="<?php echo base_url('buycart/ticket') ?>" frameborder="0"  allowfullscreen></iframe>

		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/confirmar.js?v=1') ?>" ></script>