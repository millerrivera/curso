$(document).ready(function(){

	$("#div-ticket").hide();

	$("#genera").on("click",function(){

		var nombre = $("#nombre").val();
		var email = $("#email").val();
		var direccion = $("#direccion").val();

		if(nombre == "" || direccion == "" || email == ""){
			alert("Los campos del formulario son obligatorios");
			return false;
		}

		$.ajax({
			url:$("#form-cliente").attr("action"),
			method:'POST',
			data : $("#form-cliente").serialize(),
			success:function(respuesta){
				console.log(respuesta);
			},
			dataType:'json' 
		})

	})

});