$(document).ready(function(){
	console.log("hola desde buycart");

	$(".cantidad").on("keyup",function(){
		
		var cantidad = [];
		$('input[name^="cantidad"]')
		.each(function(){
			cantidad.push($(this).val());
    	});

    	var precio = [];
    	$('input[name^="precio"]')
		.each(function(){
			precio.push($(this).val());
    	});

		var subtotal = [];

		for (var i = 0; i < cantidad.length; i++) {
			var multiplica = precio[i]*cantidad[i];
			subtotal.push(multiplica);
		}

		$('input[name^="subtotal"]')
		.each(function(index,value){
			 //index - captura la Posicion del array
			 // Value - El valor
			 // eq() -> sirve para buscar el elemento de acuerdo a la posicion
			 $('input[name="subtotal[]"]').eq(index).val(subtotal[index]);
    	});

		var total_a_pagar = 0;
    	for (var i = 0 ; i < subtotal.length; i++) {
    		total_a_pagar = total_a_pagar+subtotal[i];
    	}

    	$("#imprime_suma").empty();
    	$("#imprime_suma").append(total_a_pagar);
    	$("#suma").val(total_a_pagar);

		console.log(cantidad);
    	console.log(precio);
    	console.log(subtotal);
    	console.log(total_a_pagar);

	})


	$("#pagar").on("click",function(){
		// validar que el monto a pagar sea mayor que 0
		var suma = $("#suma").val();
		if (suma == 0) {
			alert("Usted no tiene ningun producto añadido al carrito de compras");
			return false;
		}

		$("#form-pedidos").submit();

	})


})